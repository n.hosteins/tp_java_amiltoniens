package tools;

public class OutilsMathematiques {

    /**
     * retourne un nbre aleatoire entre 2 bornes
     * @param min
     * @param max
     * @return
     */
    public int GetrandomNumber(int min, int max){
        int result = (int) (Math.random() * (max - min + 1)) + min;
        return result;
    }
}
