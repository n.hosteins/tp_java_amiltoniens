package persons;

import items.Cafe;
import items.PosteDeTravail;
import items.Programme;
import items.TypePoste;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Developpeur extends Amiltonien {
     private List<Programme> programmeList= new ArrayList<>();
    private PosteDeTravail posteDeTravailDeveloppeur = new PosteDeTravail(TypePoste.FIXE);
    private final String PREF_LANGAGE = "java";

    // Constructeurs
    public Developpeur(List<Programme> programmeList) {
        super();
        this.programmeList = programmeList;
    }
    public Developpeur() {
        super();
    }
    public Developpeur(String nom, String prenom, int moral, Cafe cafe, LocalDate of) {
        super(nom, prenom, moral, cafe, of);
    }
    public Developpeur(String nom, String prenom, LocalDate of) {
        super(nom, prenom, of);
    }
    public Developpeur(String nom, String prenom, int moral, Cafe cafe, LocalDate of, List<Programme> lp) {
        super(nom, prenom, moral, cafe, of);
        this.programmeList = lp;

    }

    // getters / setters
    public List<Programme> getProgrammeList() {
        return programmeList;
    }

    public void setProgrammeList(List<Programme> programmeList) {
        this.programmeList = programmeList;
    }

    public PosteDeTravail getPosteDeTravail() {
        return posteDeTravailDeveloppeur;
    }

    public void setPosteDeTravailDeveloppeur(PosteDeTravail posteDeTravailDeveloppeur) {
        this.posteDeTravailDeveloppeur = posteDeTravailDeveloppeur;
    }

    public String getPrefLangage() {
        return PREF_LANGAGE;
    }



    /**
     * gère l'affichage de la liste de programme
     * @return
     */
    public String showPrograms(){

        String pl = "";
        for (Programme p : programmeList){
            pl += "\t" + p + ", \n";
        }
        return pl;
    }

    /**
     * surchage la methode toString de la classe
     * @return
     */
    @Override
    public String toString() {
        return  super.toString() + " \n Il travaille sur: \n" +
               showPrograms() +
                posteDeTravailDeveloppeur +"Son langage préféré est le " + PREF_LANGAGE + ". \n";
    }

    /**
     * casse le poste de travail du développeur
     */
    public void cassePoste(){
        this.posteDeTravailDeveloppeur.setBroken(true);
    }

    /**
     * ajoute ou soustrait aléatoirement de bugs sur un programme du développeur
     * @param p le programme sur lequel on travaille
     */
    public void travailSurBug(Programme p){
        int max = p.getNbBugs();
        int newBugs = (int)(Math.random()*max);
        int op =  (int)(Math.random()*2);
        int ope;
        if (op == 1) {ope = max + newBugs; } else {ope =max - newBugs;};

        p.setNbBugs(ope);
    }

    /**
     * supprime 1 rpogramme de la liste de progammes du développeur
     * @param p le programme à supprimer
     */
    public void supprProgramme (String p){
        this.programmeList.remove(getProgram(p));
    }

    /**
     * recherche un programme dans la liste à partir de son nom
     * @param pNom nom du prgramme
     * @return le programme  complet
     */
    public Programme getProgram (String pNom){
        Programme pRecherché = new Programme();
        for (Programme pr : this.programmeList){
            if (pr.getNomProg().equals(pNom))
                pRecherché= pr;
        }
     return pRecherché;
    }

    /**
     *
     * @return une chaine de carac prédefinie
     */
    public String getLangagePrefere(){
            return "Son Langage préféré? Le Java évidemment !";
    }


}
