package persons;

public enum TypeAmiltonien {
    DEVELOPPEUR("Developpeur"),
    RESSOURCESHUMAINES("RessourcesHumaines"),
    SYSADMIN("SysAdmin");

    private String poste;

    TypeAmiltonien(String poste){
        this.poste = poste;
    }

    @Override
    public String toString() {
        return poste;
    }
}
