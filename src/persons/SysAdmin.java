package persons;

import items.Cafe;
import items.PosteDeTravail;
import items.TypePoste;

import java.time.LocalDate;

public class SysAdmin  extends Amiltonien{
    private PosteDeTravail posteDeTravail = new PosteDeTravail(TypePoste.FIXE);



    public SysAdmin(String nom, String prenom, int moral, Cafe cafe, LocalDate dateRecrutement) {
        super(nom, prenom, moral, cafe, dateRecrutement);

    }
    public SysAdmin() {}
    public SysAdmin(String nom, String prenom, LocalDate of) {
        super(nom, prenom, of);
    }


    public void repare(String nom, String prenom){
        if (Amiltonien.getAmiltonien(nom, prenom) instanceof Developpeur ){
            Developpeur am = (Developpeur) Amiltonien.getAmiltonien(nom, prenom);
            am.getPosteDeTravail().setBroken(false);
            System.out.println(this.getPrenom() + " " + this.getNom() +  " a réparé le poste de " + am.getPrenom() + " " + am.getNom());
        } else {
            System.out.println("Cette personne n'est pas un développeur");
        }

    }

    @Override
    public String toString() {
        return super.toString() +" \n En tan que SysAdmin, " + this.getPrenom() + " " + this.getNom() + " repare beaucoup de postes de travail";
    }


}
