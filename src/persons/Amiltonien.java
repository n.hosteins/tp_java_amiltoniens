package persons;

import items.Cafe;
import javafx.scene.chart.ScatterChart;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public abstract class Amiltonien {
    private String nom;
    private String prenom;
    private int moral = 100;
    private Cafe cafe = new Cafe(0, 100, 80);
    private LocalDate dateRecrutement;
    private static List<Amiltonien> amiltonienList = new ArrayList<>();

    // constructeurs
    public Amiltonien() {
    }

    public Amiltonien(String nom, String prenom, int moral, Cafe cafe, LocalDate dateRecrutement) {
        this.nom = nom;
        this.prenom = prenom;
        this.moral = moral;
        this.cafe = cafe;
        this.dateRecrutement = dateRecrutement;
        this.amiltonienList.add(this);
    }

    public Amiltonien(String nom, String prenom, LocalDate dateRecrutement) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateRecrutement = dateRecrutement;
        this.amiltonienList.add(this);
    }

    //getters / setters
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public int getMoral() {
        return moral;
    }
    public void setMoral(int moral) {
        this.moral = moral;
    }
    public Cafe getCafe() {
        return cafe;
    }
    public void setCafe(Cafe cafe) {
        this.cafe = cafe;
    }
    public LocalDate getDateRecrutement() {
        return dateRecrutement;
    }
    public void setDateRecrutement(LocalDate dateRecrutement) {
        this.dateRecrutement = dateRecrutement;
    }
    public static List<Amiltonien> getAmiltonienList() {
        return amiltonienList;
    }
    public static void setAmiltonienList(List<Amiltonien> amiltonienList) {
        Amiltonien.amiltonienList = amiltonienList;
    }

    @Override
    public String toString() {
        return "\n" + prenom + " " + nom + " a été embauché le " + dateRecrutement + ". Son moral est de " + moral + "%. " + this.cafe;
    }

    /**
     * diminue le remplissage du café de 10% lorsqu'on boit
     */
    public void boirecafe(){
        if (this.cafe.getTauxRemplissageTasse()>0){
           this.cafe.setTauxRemplissageTasse(this.cafe.getTauxRemplissageTasse()*90/100);
        }
    }

    /**
     * diminue la t° du café de 1/2 quand on souflle
     */
    public void souffler(){
        this.cafe.setTemperature(this.cafe.getTemperature()/2);
    }

    /**
     * compare l ancienneté de notre amiltonien avec un autre amiltonien entré en parametre
     */
    public void compareDateRecrutement(String nom, String prenom){
        Amiltonien am2 = Amiltonien.getAmiltonien(nom, prenom);
        String premier, deuxieme;
       if ( this.dateRecrutement.isAfter(am2.dateRecrutement)){
           premier = am2.prenom;
           deuxieme = this.prenom;
       } else {
           premier = this.prenom;
           deuxieme =  am2.prenom;
       }
        System.out.println(premier + " a été recruté avant " + deuxieme);

    }

    /**
     * retrouve uhn amiltonien dans un list avec ses noms et prenoms (methode "forEach")
     * @param nom
     * @param prenom
     */
   public static Amiltonien getAmiltonien (String nom, String prenom){
        Amiltonien present = null;
         for( Amiltonien am : amiltonienList){
             if (am.nom == nom && am.prenom == prenom){
                present = am;
             }
       }
         return present;
    }


}
