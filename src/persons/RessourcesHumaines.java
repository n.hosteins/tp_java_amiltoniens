package persons;

import items.Cafe;
import items.PosteDeTravail;
import items.TypePoste;
import java.time.LocalDate;

public class RessourcesHumaines extends Amiltonien{

    private PosteDeTravail posteDeTravailRH = new PosteDeTravail(TypePoste.PORTABLE);

    //constructeurs
    public RessourcesHumaines(String nom, String prenom, int moral, Cafe cafe, LocalDate dateRecrutement) {
        super(nom, prenom, moral, cafe, dateRecrutement);
     ;
    }
    public RessourcesHumaines() { }
    public RessourcesHumaines(String nom, String prenom, LocalDate of) {
        super(nom, prenom, of);
    }

    public void setPosteDeTravail(PosteDeTravail posteDeTravailRH) {
        this.posteDeTravailRH = posteDeTravailRH;
    }

    /**
     * redonne 10% de moral à un amiltonien si il a moins de 90% de moral
     */
    public void boostMoral (String nom, String prenom){
        Amiltonien am =Amiltonien.getAmiltonien(nom, prenom);
        if (am.getMoral() < 90){
            int newMoral = am.getMoral() + 10;
            if (newMoral > 100){newMoral = 100;}
            am.setMoral(newMoral);
        };
    }

   public void recrutement( TypeAmiltonien fonction, String nom, String prenom, LocalDate date){
       AmiltonienFactory.newAmiltonien(fonction, nom, prenom, date);

    }
}
