package persons;

import java.time.LocalDate;

public class AmiltonienFactory {
    public static Amiltonien newAmiltonien (TypeAmiltonien fonction, String nom, String prenom, LocalDate ld){
        if (TypeAmiltonien.DEVELOPPEUR.equals(fonction)){
            return new Developpeur(nom, prenom, ld);
        } else if  (TypeAmiltonien.RESSOURCESHUMAINES.equals(fonction)){
            return new RessourcesHumaines(nom, prenom, ld);
        } else if  (TypeAmiltonien.SYSADMIN.equals(fonction)){
            return new SysAdmin(nom, prenom, ld);
        } else {
            System.out.println("non!");
        }
        return null;
    }


}
