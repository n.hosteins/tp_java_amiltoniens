package items;

import java.text.DecimalFormat;

public class Cafe {
    private int nbSucres;
    private double tauxRemplissageTasse;
    private int temperature;
    private boolean tropChaud;

    public Cafe(int nbSucres, double tauxRemplissageTasse, int temperature) {
        this.nbSucres = nbSucres;
        this.tauxRemplissageTasse = tauxRemplissageTasse ;
        this.temperature = temperature;
       isChaud();
    }

    public Cafe() {}

    public int getNbSucres() {
        return nbSucres;
    }

    public void setNbSucres(int nbSucres) {
        this.nbSucres = nbSucres;
    }

    public double getTauxRemplissageTasse() {
        return tauxRemplissageTasse;
    }

    public void setTauxRemplissageTasse(double tauxRemplissageTasse) {
        this.tauxRemplissageTasse = tauxRemplissageTasse;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
        isChaud();

    }

    public boolean isTropChaud() {
        return tropChaud;
    }


    public boolean isEmpty(){
        boolean plein = false;
        if (this.tauxRemplissageTasse == 100){
            plein = true;
        }
        return plein;
    }

    public void isChaud(){
        if (temperature >= 100) {
            this.tropChaud = true;
        } else {
            this.tropChaud = false;
        }
    }

    @Override
    public String toString() {
        String chaud;
        if (tropChaud) {
            chaud = "il est trop chaud.";
        } else {
            chaud = "il n'est pas trop chaud.";
        };
        return " Le Cafe a " + nbSucres + " sucres et est rempli à " + new DecimalFormat("#.##").format(tauxRemplissageTasse) + "%. Sa temperature est de " + temperature + "°C, "
                + chaud;
    }
}
