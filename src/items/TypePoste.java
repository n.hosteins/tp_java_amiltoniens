package items;

public enum TypePoste {
    FIXE("fixe"),
    PORTABLE("portable");

 private String poste;

    TypePoste(String poste){
        this.poste = poste;
    }

    @Override
    public String toString() {
        return poste;
    }
}
