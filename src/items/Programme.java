package items;

public class Programme {
    private String nomProg;
    private int nbBugs;

    public Programme(String nomProg, int nbBugs) {
        this.nomProg = nomProg;
        this.nbBugs = nbBugs;
    }

    public Programme() {
    }

    public String getNomProg() {
        return nomProg;
    }

    public void setNomProg(String nomProg) {
        this.nomProg = nomProg;
    }

    public int getNbBugs() {
        return nbBugs;
    }

    public void setNbBugs(int nbBugs) {
        this.nbBugs = nbBugs;
    }

    @Override
    public String toString() {
        return "Le programme " + nomProg +
                " a " + nbBugs + " bug(s)";
    }
}
