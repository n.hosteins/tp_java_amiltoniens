package items;

public class PosteDeTravail {
   TypePoste poste;
    boolean isBroken = false;

    public PosteDeTravail(TypePoste poste, boolean isBroken) {
        this.poste = poste;
        this.isBroken = isBroken;
    }

    public PosteDeTravail(TypePoste poste) {
        this.poste = poste;
    }

    public PosteDeTravail() {
    }

    public TypePoste getPoste() {
        return poste;
    }

    public void setPoste(TypePoste poste) {
        this.poste = poste;
    }

    public boolean isBroken() {
        return isBroken;
    }

    public void setBroken(boolean broken) {
        isBroken = broken;
    }

    @Override
    public String toString() {
        String str = isBroken?  "il est cassé":  "il n'est pas cassé";
        return " Le poste de travail est un poste " +
                this.poste.toString() + " et "
                 +str + ". \n";
    }
}
