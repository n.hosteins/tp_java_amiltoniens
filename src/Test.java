import items.Cafe;
import items.PosteDeTravail;
import items.Programme;
import items.TypePoste;


import persons.*;
import tools.OutilsMathematiques;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class Test {
    public static void main(String[] args) {
        Cafe cafe = new Cafe(2, 50, 120);
        Programme prog = new Programme("amiltoniens", 12);
        Programme prog2 = new Programme("amiltoniens2", 1);
        List<Programme> lp = new ArrayList<>();
        lp.add(prog);
        lp.add(prog2);
        new RessourcesHumaines().recrutement(TypeAmiltonien.RESSOURCESHUMAINES,"Annie", "Steph", LocalDate.of(2018, 03 ,10));
        System.out.println(Amiltonien.getAmiltonien("Annie", "Steph"));
        RessourcesHumaines steph = (RessourcesHumaines)Amiltonien.getAmiltonien("Annie", "Steph");
        //steph.recrutement(TypeAmiltonien.RESSOURCESHUMAINES,"Dafon", "Leo", LocalDate.of(2018, 03 ,10));
        //System.out.println(Amiltonien.getAmiltonien("Dafon", "Leo").getCafe());
    }
}
